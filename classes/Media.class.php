<?php
include_once "Db.class.php";
class Media

{
    private $m_iFiD;
    private $m_sfragmentNaam;
    private $m_sAfleveringsNr;
    private $m_sType;
    private $m_dDatumUitzending;
    private $m_sSamenvatting;
    private $m_sPersonages;
    private $m_sMediaUrl;


    function __SET($p_sProperty, $p_vValue)
    {
        switch ($p_sProperty) {
            case "FiD":
                $this->m_iFiD = $p_vValue;
                break;
            case "Naam":
                $this->m_sfragmentNaam = $p_vValue;
                break;
            case "AfleveringNr":
                $this->m_sAfleveringsNr = $p_vValue;
                break;
            case "Type":
                $this->m_sType = $p_vValue;
                break;
            case "Datum":
                $this->m_dDatumUitzending = $p_vValue;
                break;
            case "Samenvatting":
                $this->m_sSamenvatting = $p_vValue;
                break;
            case "Personages":
                $this->m_sPersonages = $p_vValue;
                break;
            case "Media":
                $this->m_sMediaUrl = $p_vValue;
                break;
        }
    }

    function __GET($p_sProperty)
    {
        switch ($p_sProperty){
            case "FiD":
                return $this->m_iFiD;
                break;
            case "Naam":
                return $this->m_sfragmentNaam;
                break;
            case "AfleveringNr":
                return $this->m_sAfleveringsNr;
                break;
            case "Type":
                return $this->m_sType;
                break;
            case "Datum":
                return $this->m_dDatumUitzending;
                break;
            case "Samenvatting":
                return $this->m_sSamenvatting;
                break;
            case "Personages":
                return $this->m_sPersonages;
                break;
            case "Media":
                return $this->m_sMediaUrl;
                break;
        }
    }

    public function create(){

    }

    public function getMediaList(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblMedia ORDER BY `datum` DESC");
        $stmt->execute();
        $mediaList=$stmt->fetchAll();
        return $mediaList;
    }

    public function getMedia(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE `fragmentNr` =:id");
        $stmt->bindparam(":id", $this->m_iFiD);
        $stmt->execute();
        $media=$stmt->fetchAll();
        return $media;
    }


    public function choseContent2($p_jaar,$p_personage,$p_event, $p_type){
        $conn = Db::getInstance();
        if($p_personage === "Alle" && $p_event ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);

        }elseif($p_personage ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `event`=:event ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $stmt->bindparam(":event", $p_event);
        }elseif($p_event ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `personages` LIKE :personage ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
        }else{
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `event`=:event AND `personages` LIKE :personage ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
            $stmt->bindparam(":event", $p_event);
        }


        $stmt->execute();

        $media=$stmt->fetchAll();
        return $media;
    }
	public function getShares(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT `fragmentNr`,`event`,`title`,`shares` FROM `tblMedia` ORDER BY `shares` DESC LIMIT 10");
        $stmt->execute();
        $media=$stmt->fetchAll();
        return $media;
	}
    public function getViews(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT `fragmentNr`,`event`,`title`,`views` FROM `tblMedia` ORDER BY `views` DESC LIMIT 10");
        $stmt->execute();
        $media=$stmt->fetchAll();
        return $media;
    }

    public function addView(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("UPDATE tblMedia SET views= views + 1 WHERE fragmentNr=:id");
        $stmt->bindparam(":id", $this->m_iFiD);
        $stmt->execute();
    }
    public function addShare(){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("UPDATE tblMedia SET shares= shares + 1 WHERE fragmentNr=:id");
        $stmt->bindparam(":id", $this->m_iFiD);
        $stmt->execute();
    }
	
    public function choseContent($p_jaar,$p_personage,$p_event, $p_type){
        $conn = Db::getInstance();
        if($p_personage === "Alle" && $p_event ==="Alle" && $p_type === "Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);

        }elseif($p_personage ==="Alle" && $p_type === "Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `event`=:event ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $stmt->bindparam(":event", $p_event);
        }elseif($p_event ==="Alle" && $p_type === "Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `personages` LIKE :personage ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
        }elseif($p_event ==="Alle" && $p_personage === "Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `type`=:typee ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $stmt->bindparam(":typee", $p_type);
        }elseif($p_event ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `personages` LIKE :personage AND `type`=:typee ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
            $stmt->bindparam(":typee", $p_type);
        }elseif($p_type ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `personages` LIKE :personage AND `event`=:event ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
            $stmt->bindparam(":event", $p_event);
        }elseif($p_personage ==="Alle"){
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `type`=:typee AND `event`=:event ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);

            $stmt->bindparam(":typee", $p_type);
            $stmt->bindparam(":event", $p_event);
        }else{
            $stmt = $conn->prepare("SELECT * FROM tblMedia WHERE YEAR(`datum`) =:jaar AND `event`=:event AND `personages` LIKE :personage ORDER BY `datum` DESC");
            $stmt->bindparam(":jaar", $p_jaar);
            $p_personage = "%".$p_personage."%";
            $stmt->bindparam(":personage", $p_personage);
            $stmt->bindparam(":event", $p_event);
        }


        $stmt->execute();

        $media=$stmt->fetchAll();
        return $media;
    }



}


