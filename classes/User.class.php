<?php
include_once "Db.class.php";
class User{

    private $m_sGeslacht;
	private $m_sEmail;
	private $m_sGeboorteDatum;
    private $m_sPassword;

    function __SET($p_sProperty, $p_vValue){
        switch ($p_sProperty){
            case "Email":
                $this->m_sEmail = $p_vValue;
                break;
            case "Password":
                $this->m_sPassword = $p_vValue;
                break;
            case "Geslacht":
                $this->m_sGeslacht = $p_vValue;
                break;
            case "Geboortedatum":
                $this->m_sGeboorteDatum = $p_vValue;
                break;
        }
    }
    function returnAll(){
        return  $this->m_sEmail;
    }

    public function emailIsUnique($p_email){
        $conn = Db::getInstance();
        $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
        $stmt->bindparam(":email", $p_email);
        $stmt->execute();
        if($stmt->rowCount() !== 0){
            return false;
        }
        return true;
    }

    public function login($email, $password)
    {

            $conn = Db::getInstance();
            $new_password = password_hash($password, PASSWORD_DEFAULT);
            echo $new_password;
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
            $stmt->bindparam(":email", $email);
            $stmt->execute();
            $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

            if($stmt->rowCount() > 0)
            {
                echo "<br>". $userRow['wachtwoord'];
                if(password_verify($new_password, $userRow['wachtwoord']))
                {
                    session_start();
                    $_SESSION['admin'] = $userRow['admin'];
                    $_SESSION['loggedin'] = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }

    }

    public function checkAdmin($p_email){
        try{
            $conn = Db::getInstance();
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email AND admin=1");
            $stmt->bindparam(":email", $p_email);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            }else{
                return false;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function userExists(){
        try{
            $conn = Db::getInstance();
            $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
            $stmt->bindparam(":email", $this->m_sEmail);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            }else{
                return false;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}
?>