<?php

class Db
{
    private static $conn;

    public static function getInstance(){
        if( is_null( self::$conn ) ){
            try{
                self::$conn = new PDO("mysql:host=63.143.48.153; dbname=imdstagram_thuisapp", "imdstagram", "imdstagram");
            } catch(PDOException $e){
                echo $e->getMessage();
            }
        }
        return self::$conn;
    }
}

?>