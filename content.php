<?php

include_once('classes/Media.class.php');
include_once ('classes/Db.class.php');
session_start();

$conn = Db::getInstance();
$stmt = $conn->prepare("SELECT `fragmentNr`,`event`,`title`,`shares` FROM `tblMedia` ORDER BY `shares` DESC LIMIT 10");
$stmt->execute();
$media=$stmt->fetchAll();




?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin: overview</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
    <link rel="stylesheet" href="css/timeline.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body id="admin">
<?php include_once('includes/nav2.php'); ?>
<div class="form_login card-panel hoverable">
    <h4>Meest gedeelde items</h4>


    <table class="highlighted">
        <thead>
        <tr>
            <th data-field="id">Gebeurtenis</th>
            <th data-field="name">Titel</th>
            <th data-field="price">Aantal shares</th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach($media as $mediaItem) { ?>

            <tr>
                <td><?php echo $mediaItem['event']; ?></td>
                <td><a href="content.php?id=<?php echo $mediaItem['fragmentNr']; ?>"><?php echo $mediaItem['title']; ?></td>
                <td><?php echo $mediaItem['shares']; ?></td>
            </tr>

        <?php } ?>
        </tbody>
    </table>

</div>
</div>
</div>
</body>
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>

</html>