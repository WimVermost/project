<?php

	include_once "classes/User.class.php";
    session_start();

    if(isset($_POST['register'])){


            if ($_POST['geboortedatum'] == "0000-00-00") {
                $feedback = "Gelieve een geldige voornaam te gebruiken";
            }
             if ($_POST['geslacht'] == "") {
                 $feedback = "Gelieve een geslacht aan te duiden.";
             }
            elseif ($_POST['email'] == "") {
                $feedback = "Gelieve een geldige email te gebruiken";
            }
            elseif ($_POST['wachtwoord'] == "") {
                $feedback = "Uw wachtwoord mag niet leeg zijn";
            }
            else {

                $conn = Db::getInstance();
                $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
                $stmt->bindparam(":email", $_POST['email']);
                $stmt->execute();
                if ($stmt->rowCount() == 0) {

                    $new_password = password_hash($_POST['wachtwoord'], PASSWORD_DEFAULT);
                    $conn = Db::getInstance();
                    $stmt = $conn->prepare("INSERT INTO `tblUsers`( `geboortedatum`, `geslacht`, `email`, `wachtwoord`, `admin`) VALUES (:geboortedatum,:geslacht,:email,:wachtwoord,0)");

                    $stmt->bindparam(":geboortedatum", $_POST['geboortedatum']);
                    $stmt->bindparam(":geslacht", $_POST['geslacht']);
                    $stmt->bindparam(":email", $_POST['email']);
                    $stmt->bindparam(":wachtwoord", $new_password);
                    if ($stmt->execute()) {
                        $_SESSION['email'] = $_POST['email'];
                        $_SESSION['loggedin'] = true;
                        header('location: index.php');
                    }
                }
                else{
                    $feedback = "Gebruiker bestaat al.";
                }
            }


		
    }

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registreer voor toegang tot de Thuis Tijdlijn</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
    <link rel="stylesheet" href="css/timeline.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php include_once('includes/nav.php'); ?>
<div class="body_login">
<div class="form_login card-panel hoverable">

    <form method="POST" action="registreer.php" class="col s12">



      <div class="row">
      	<div class="input-field col s12">
			  <select class="browser-default" name="geslacht">
				<option value="" disabled selected>Kies je geslacht</option>
				<option value="man">Man</option>
				<option value="vrouw">Vrouw</option>
			  </select>
		  </div>   	
      </div>

        <div class="row">
            <div class="input-field col s12">
                <input type="date" name="geboortedatum" >
            </div>
        </div>
           
      <div class="row">
        <div class="input-field col s12">
          <input id="email" type="email" class="validate" name="email">
          <label for="email" data-error="Gelieve een correcte email te gebruiken">Email</label>
        </div>
      </div> 
      
      <div class="row">
        <div class="input-field col s12">
          <input id="password" name="wachtwoord" type="password" class="validate">
          <label for="password">Password</label>
        </div>
      </div>
      
      <div class="row">
      	<input type="submit" class="waves-effect waves-light btn-large" name="register">
      </div>

    </form>
   
</div>
</div>
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>
</body>
</html>