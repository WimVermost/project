<?php

include_once "classes/Db.class.php";
session_start();

if(isset($_POST['login'])){

    $password = $_POST['pass'];
    $email = htmlspecialchars($_POST['email']);


    $conn = Db::getInstance();


    $stmt = $conn->prepare("SELECT * FROM tblUsers WHERE email=:email");
    $stmt->bindparam(":email", $email);
    $stmt->execute();
    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    if($stmt->rowCount() > 0)
    {
        echo "<br>".$userRow['wachtwoord'];
        if(password_verify($password, $userRow['wachtwoord']))
        {


            echo "ok!";
            $_SESSION['admin'] = $userRow['admin'];
            $_SESSION['email'] = $email;
            $_SESSION['loggedin'] = true;
            header('location: index.php');

        }
        else
        {
            echo "<br>failed";

        }
    }

}

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thuis Webapp</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
    <link rel="stylesheet" href="css/timeline.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php include_once('includes/nav.php'); ?>
<div class="body_login">
<div class="form_login card-panel hoverable">

    <form class="col s12" method="POST" action="login.php">

      <?php if(isset($message)){
          echo "<div class='row'>$message</div>";
      }?>

	  <div class="row">
        <div class="input-field col s12">
          <input id="email" name="email" type="email" class="validate">
          <label for="email" data-error="Gelieve een correcte email te gebruiken">Email</label>
        </div>
      </div> 
      
      <div class="row">
        <div class="input-field col s12">
          <input id="password" name="pass" type="password" class="validate">
          <label for="password">Password</label>
        </div>
      </div>
      
      <div class="row">
          <input type="submit" class="waves-effect waves-light btn-large" name="login">
      </div>
    </form>
   
</div>
</div>
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>
</body>
</html>