<?php
include_once('classes/Media.class.php');
session_start();


$media = new Media();
$mediaList = $media->getMediaList();

//if(isset($_POST['btn-filter'])) {
//    $jaar = $_POST['select-jaar'];
//    $person = $_POST['select-person'];
//    $event = $_POST['select-event'];
//
//    $mediaList = $media->choseContent(2016,"Frank","Alle");

//}


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thuis Webapp</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
    <link rel="stylesheet" href="css/timeline.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php include_once('includes/nav2.php'); ?>

<main>

    <div id="container">
        <section id="timeline">
            <div class="btn-filter-open">
                <input id="btn-filter-close" name="close-btn" type="button" value="Filter" class="waves-effect waves-light teal lighten-2 ">
            </div>
            <div class="mobile-filter">
                <form action="index.php" class="form-filter" method="post">
                    <div class="selector">
                        <label>Type</label>
                        <select id="select-type" name="select-type" class="browser-default selector-type">
                            <option value="Alle"  selected>Alle</option>
                            <option value="video">Video</option>
                            <option value="photo">Afbeelding</option>
                            <option value="quote">Quote</option>
                        </select>
                    </div>

                    <div class="selector">
                        <label>Personage</label>
                        <select id="select-person" name="select-person" class="browser-default selector-person">
                            <option value="Alle"  selected>Alle</option>
                            <option value="Simonne">Simonne</option>
                            <option value="Frank">Frank</option>
                            <option value="Nancy">Nancy</option>
                            <option value="Karin">Karin</option>
                            <option value="Peggy">Peggy</option>
                        </select>
                    </div>
                    <div class="selector">
                        <label>Evenementen</label>
                        <select id="select-event" name="select-event" class="browser-default selector-event">
                            <option value="Alle"  selected>Alle</option>
                            <option value="Dood">Dood</option>
                            <option value="Verkrachting">Verkrachting</option>
                            <option value="Overspel">Overspel</option>
                            <option value="Relatie">Relatie</option>
                            <option value="Ruzie">Ruzie</option>
                        </select>

                    </div>
                    <div class="selector">
                        <label>Jaar</label>
                        <select name="select-jaar" id="jaartallen1" class="browser-default selector-season">
                        </select>
                    </div>
                    <input id="btn-filter1" name="filter-btn" type="button" value="filter" class="working-btn btn teal lighten-2 ">
                    <div class="btn-filter-close">
                        <input id="btn-filter-close" name="close-btn" type="button" value="verbergen" class="waves-effect waves-light teal lighten-2 ">
                    </div>
                </form>
            </div>
            <section id="cd-timeline" class="cd-container">

                <?php

                    foreach($mediaList as $mediaItem) {
                        ?>

                        <div class="cd-timeline-block">


                                <?php

                                switch ($mediaItem['type']) {
                                    case "video":
                                        echo "<div class='cd-timeline-img cd-movie'>";
                                        echo "<img src='images/icons/icon-movie.svg'
                                     alt='Movie'>";
                                        break;
                                    case "photo":
                                        echo "<div class='cd-timeline-img cd-picture'>";
                                        echo "<img src='images/icons/icon-picture.svg'
                                     alt='Photo'>";
                                        break;
                                    case "quote":
                                        echo "<div class='cd-timeline-img cd-quote'>";
                                        echo "<img src='images/icons/icon-quote.svg'
                                     alt='Quote'>";
                                        break;
                                }
                                ?>

                            </div> <!-- image type-->

                            <div id="media_<?php echo $mediaItem['fragmentNr']; ?>" class="cd-timeline-content">
                                <div class="overlay-black"></div>
                                <img src="images/covers/<?php echo $mediaItem['cover'];?>" alt="">
                                <div class="white-text pushup">
                                    <div>
                                        <div class="content-title">
                                            <h2 class="white-text">Aflevering <?php echo $mediaItem['afleveringNr'];?></h2>
                                            <span class="cd-date"><?php echo $mediaItem['datum'];?></span>
                                        </div>
                                        <p><?php echo $mediaItem['title'];?></p>
                                    </div>
                                    <a href="#0" class="cd-timeline-play"><img src="images/play.svg" alt=""></a>

                                </div>
                            </div>

                <!-- cd-timeline-content -->
                        </div> <!-- cd-timeline-block -->

                        <?php
                    }
                ?>

            </section> <!-- cd-timeline -->

        </section>
        <section id="info">
            <!-- filter -->
            <section class="topwrapper">
                <div id="top">
                    <form action="index.php" class="form-filter" method="post">
                        <div class="selector">
                            <label>Type</label>
                            <select id="select-type2" name="select-type" class="browser-default selector-type">
                                <option value="Alle"  selected>Alle</option>
                                <option value="video">Video</option>
                                <option value="photo">Afbeelding</option>
                                <option value="quote">Quote</option>
                            </select>
                        </div>

                        <div class="selector">
                            <label>Personage</label>
                            <select id="select-person2" name="select-person" class="browser-default selector-person">
                                <option value="Alle"  selected>Alle</option>
                                <option value="Simonne">Simonne</option>
                                <option value="Frank">Frank</option>
                                <option value="Nancy">Nancy</option>
                                <option value="Karin">Karin</option>
                                <option value="Peggy">Peggy</option>
                            </select>
                        </div>
                        <div class="selector">
                            <label>Evenementen</label>
                            <select id="select-event2" name="select-event" class="browser-default selector-event">
                                <option value="Alle"  selected>Alle</option>
                                <option value="Dood">Dood</option>
                                <option value="Verkrachting">Verkrachting</option>
                                <option value="Overspel">Overspel</option>
                                <option value="Relatie">Relatie</option>
                                <option value="Ruzie">Ruzie</option>
                            </select>

                        </div>
                        <div class="selector">
                            <label>Jaar</label>
                            <select name="select-jaar" id="jaartallen2" class="browser-default selector-season2">
                            </select>
                        </div>
                        <input id="btn-filter2" name="filter-btn" type="button" value="filter" class="waves-effect btn-filter waves-light btn teal lighten-2 ">
                    </form>
                </div>
            </section>
            <!-- content -->
            <?php

            $media->FiD = 1;
            $array = $media->getMedia();

            ?>
           <section class="welcome-msg"><h3>Welkom op de Thuis App!</h3><h5>Klik op die tijdlijn om te beginnen.</h5></section>
            <section class="content-block">
                <div class="btn-back">
                    <input id="btn-back" name="back-btn" type="button" value="Ga terug" class="waves-effect waves-light teal lighten-2 ">
                </div>
                <div class="video">
                    <div class="block" id="left">
                        <div class="share-hidden"><input class="hiddenId" type="hidden" value="<?php echo $array[0][0];?>"></div>
                        <div id="content-title"><h4></h4></div>
                        <div class="personages">



                        </div>
                        <div id="content-description"><p></p></div>
                    </div>
                    <div class="block" id="right">
                        <div id="content-source">

                        </div>
                    </div>
                </div>
                <div class="quote">

                </div>


                <!-- social media share -->
                <div id="rightD">
                    <div id="share"></div>
                </div>
            </section>
        </section>
    </div>
</main>

<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>
</body>
</html>