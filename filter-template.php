<section id="cd-timeline" class="cd-container">
    <?php



    $mediaList = $_POST['array'];

    foreach($mediaList as $mediaItem) {
        ?>

        <div class="cd-timeline-block">
            <div class="cd-timeline-img cd-movie">
                <?php

                switch ($mediaItem['type']) {
                    case "video":
                        echo "<img src='images/icons/icon-movie.svg'
                                     alt='Movie'>";
                        break;
                    case "photo":
                        echo "<img src='images/icons/icon-picture.svg'
                       alt='Photo'>";
                        break;
                    case "quote":
                        echo "<img src='images/icons/icon-quote.svg'
                       alt='Quote'>";
                        break;
                }
                ?>

            </div> <!-- image type-->
            <div id="media_<?php echo $mediaItem['fragmentNr']; ?>" class="cd-timeline-content">
                <div class="overlay-black"></div>
                <img src="images/covers/<?php echo $mediaItem['cover'];?>" alt="">
                <div class="white-text pushup">
                    <div>
                        <div class="content-title">
                            <h2 class="white-text">Aflevering blab <?php echo $mediaItem['afleveringNr'];?></h2>
                            <span class="cd-date"><?php echo $mediaItem['datum'];?></span>
                        </div>
                        <p><?php echo $mediaItem['title'];?></p>
                    </div>
                    <a href="#0" class="cd-timeline-play"><img src="images/play.svg" alt=""></a>

                </div>
            </div> <!-- cd-timeline-content -->
        </div> <!-- cd-timeline-block -->

        <?php
    }
    ?>

</section> <!-- cd-timeline -->
