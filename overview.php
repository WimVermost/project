<?php

	error_reporting(0);

include_once('classes/Media.class.php');
include_once ('classes/Db.class.php');
session_start();

$conn = Db::getInstance();
$stmt = $conn->prepare("SELECT `fragmentNr`,`event`,`title`,`shares` FROM `tblMedia` ORDER BY `shares` DESC LIMIT 10");
$stmt->execute();
$media=$stmt->fetchAll();


$stmt = $conn->prepare("SELECT AVG( TIMESTAMPDIFF( YEAR,  `geboortedatum` , NOW( ) ) ) AS  `average`
FROM  `tblUsers`");
$stmt->execute();
$age = $stmt->fetchAll();
$age = round($age[0]["average"]);


$stmt = $conn->prepare("SELECT AVG( TIMESTAMPDIFF( YEAR,  `geboortedatum` , NOW( ) ) ) AS  `average`
FROM  `tblUsers` WHERE `geslacht` = 'man'");
$stmt->execute();
$ageman = $stmt->fetchAll();
$ageman = round($ageman[0]['average']);

$stmt = $conn->prepare("SELECT AVG( TIMESTAMPDIFF( YEAR,  `geboortedatum` , NOW( ) ) ) AS  `average`
FROM  `tblUsers` WHERE `geslacht` = 'vrouw'");
$stmt->execute();
$agevrouw = $stmt->fetchAll();
$agevrouw = round($agevrouw[0]['average']);


$stmt = $conn->prepare("select count(case geslacht when 'man' then 1 else null end) from tblUsers");
$stmt->execute();
$aantalmannen=$stmt->fetch();
$aantalmannen = $aantalmannen[0];

$stmt = $conn->prepare("select count(case geslacht when 'vrouw' then 1 else null end) from tblUsers");
$stmt->execute();
$aantalvrouwen=$stmt->fetch();
$aantalvrouwen = $aantalvrouwen[0];


$totaal = $aantalvrouwen + $aantalmannen;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin: overview</title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
	<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
	<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
	<link rel="stylesheet" href="css/timeline.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="admin">
	<?php include_once('includes/nav2.php'); ?>
	<?php //if($user->checkAdmin($_SESSION['email'])){?>
		<div class="form_login card-panel hoverable" style="width:80%;">
			<div class="row overview-nav">
				<div class="col s12">
					<ul class="tabs ">
						<li class="tab col s3 overview-tab" id="shares"><a class="active" href="#test1">Shares</a></li>
						<li class="tab col s3 overview-tab" id="views"><a href="#test2">Views</a></li>
					</ul>
				</div>
			</div>
			<div class="overview-block">
			<h5>Meest gedeelde items</h5>
			<table class="highlighted">
			<thead>
			  <tr>
				  <th data-field="fragmentNr">Fragment nummer</th>
				  <th data-field="id">Gebeurtenis</th>
				  <th data-field="name">Titel</th>
				  <th data-field="price">Aantal shares</th>
			  </tr>
			</thead>
			<tbody>
			<?php

			foreach($media as $mediaItem) { ?>

			  <tr>
				  <td><?php echo $mediaItem['fragmentNr']; ?></td>
				<td><?php echo $mediaItem['event']; ?></td>
				<td><?php echo $mediaItem['title']; ?></td>
				<td><?php echo $mediaItem['shares']; ?></td>

			  </tr>

			<?php } ?>
			</tbody>
		  </table></div>
			<div class="demography">
			<h5>Demografie</h5>
			<table class="highlighted">
				<thead>
				<tr>
					<th data-field="man">Mannen</th>
					<th data-field="vrouw">Vrouwen</th>
					<th data-field="totaal">Totaal</th>
				</tr>
				</thead>
				<tbody>

				<tr>
					<td> <?php echo $aantalmannen; ?> </td>
					<td> <?php echo $aantalvrouwen; ?> </td>
					<td> <?php echo $totaal; ?> </td>
				</tr>
				<tr>
					<td> <?php echo round((100/($totaal))*$aantalmannen); ?>% </td>
					<td> <?php echo round((100/($totaal))*$aantalvrouwen); ?>% </td>
					<td> 100% </td>
				</tr>

				</tbody>
			</table>

		</div>

			<div class="age">

				<h5>Leeftijd</h5>
				<table class="highlighted">
					<thead>
					<tr>
						<th data-field="vrouw">Mannen</th>
						<th data-field="vrouw">Vrouwen</th>
						<th data-field="vrouw">Gemiddelde leeftijd</th>
					</tr>
					</thead>
					<tbody>

					<tr>
						<td> <?php echo $ageman; ?></td>
						<td> <?php echo $agevrouw; ?></td>
						<td> <?php echo $age; ?></td>
					</tr>

					</tbody>
				</table>


			</div>


		</div>
	</div>
	</div>

<?php //} ?>

</body>
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>

</html>