<?php
include_once "classes/User.class.php";
session_start();
$path = "images/covers/";
$pathvideo = "videos/";
$pathgif = "images/afbeeldingen/";

$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "mp4");

	if(isset($_POST['uploadButton'])) {

		$type = $_POST['type'];
		$number = $_POST['nummer'];
		$titel = $_POST['titel'];
		$event = $_POST['event'];
		$datum = $_POST['datum'];
		$personages = $_POST['personages'];

		$samenvatting = $_POST['samenvatting'];



			$name = $_FILES['photoimg']['name'];
			$size = $_FILES['photoimg']['size'];
			if (strlen($name)) {

				$array = explode(".", $name);
				$size = count($array) - 1;
				$ext = $array[$size];

				if (in_array($ext, $valid_formats)) {
					$imagename = time();
					$tmp = $_FILES['photoimg']['tmp_name'];
					move_uploaded_file($tmp, $path . $imagename);

				}
			}

		if (strpos($type, 'quote') == false) {

			$name2 = $_FILES['media']['name'];
			$size2 = $_FILES['media']['size'];
			if (strlen($name2)) {

				$array = explode(".", $name2);
				$size2 = count($array) - 1;
				$ext = $array[$size2];

				if (in_array($ext, $valid_formats)) {
					$imagename2 = time();
					$tmp = $_FILES['media']['tmp_name'];

					if (strpos($type, 'photo') !== false) {
						move_uploaded_file($tmp, $pathgif . $imagename2);
					} else {
						move_uploaded_file($tmp, $pathvideo . $imagename2);
					}

				}
			}
			$imagename2 = "/afbeeldingen/".$imagename2;
			$conn = Db::getInstance();
			$stmt = $conn->prepare("INSERT INTO `tblMedia`(`afleveringNr`, `type`, `cover`, `title`, `datum`, `samenvatting`, `personages`, `event`, `media`, `shares`, `views`)VALUES (:number,:type,:imagename,:titel,:datum,:samenvatting,:personages,:event,:imagename2, 0,0)");
			$stmt->bindparam(":number", $number);
			$stmt->bindparam(":type", $type);
			$stmt->bindparam(":imagename", $imagename);
			$stmt->bindparam(":titel", $titel);
			$stmt->bindparam(":datum", $datum);
			$stmt->bindparam(":samenvatting", $samenvatting);
			$stmt->bindparam(":personages", $personages);
			$stmt->bindparam(":event", $event);
			$stmt->bindparam(":imagename2", $imagename2);

			if($stmt->execute()){
				header('location: index.php');
			}else{
				//echo "does not work";
			}



		}else{
			$imagename2 = "";
			$conn = Db::getInstance();
			$stmt = $conn->prepare("INSERT INTO `tblMedia`(`afleveringNr`, `type`, `cover`, `title`, `datum`, `samenvatting`, `personages`, `event`, `media`, `shares`, `views`)VALUES (:number,:type,:imagename,:titel,:datum,:samenvatting,:personages,:event,:imagename2, 0,0)");
			$stmt->bindparam(":number", $number);
			$stmt->bindparam(":type", $type);
			$stmt->bindparam(":imagename", $imagename);
			$stmt->bindparam(":titel", $titel);
			$stmt->bindparam(":datum", $datum);
			$stmt->bindparam(":samenvatting", $samenvatting);
			$stmt->bindparam(":personages", $personages);
			$stmt->bindparam(":event", $event);
			$stmt->bindparam(":imagename2", $imagename2);

			if($stmt->execute()){
				header('location: index.php');
			}else{
				//echo "does not work";
			}




		}
	}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin: content toevoegen</title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
	<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials.css" />
	<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.1.0/jssocials-theme-flat.css" />
	<link rel="stylesheet" href="css/timeline.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body id="admin">
	<?php include_once('includes/nav2.php'); ?>

	<?php //if($user->checkAdmin($_SESSION['email'])){?>
		<div class="form_login card-panel hoverable">
			<h4>Content toevoegen</h4>
				<form class="col s12" action="admin.php" method="post"  enctype="multipart/form-data">
					<div class="row">
						<div class="col s12">
							<label>Type media</label>
							<select class="browser-default" name="type">
								<option value="" disabled selected>Kies een type media</option>
								<option value="video">Video</option>
								<option value="quote">Quote</option>
								<option value="photo">Foto</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Event</label>
							<select class="browser-default" name="event">
								<option value="" disabled selected>Kies een event</option>
								<option value="Relatie">Relatie</option>
								<option value="Ruzie">Ruzie</option>
								<option value="Overspel">Overspel</option>
								<option value="Dood">Dood</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Datum van uitzending</label>
							<input type="date" name="datum">
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<label>Personages</label>
							<input type="text" name="personages">
						</div>
					</div>
					<div class="row">
						<div class="col s3">
							<label>Afleveringnummer</label>
							<input type="number" name="nummer">
						</div>
						<div class="col s9">
							<label>Titel</label>
							<input type="text" name="titel">
						</div>
					</div>
					<div class="row">
						<div class="file-field input-field">
							<div class="btn">
								<span>Cover toevoegen</span>
								<input type="file" name="photoimg" id="photoimg" class="choosefilebutton"/>
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
						<textarea id="samenvatting" class="materialize-textarea" length="150" name="samenvatting"></textarea>
						<label for="samenvatting">Korte samenvatting</label>
					</div>
					</div>

			<div class="row">
				<div class="file-field input-field">
					<div class="btn">
						<span>Media toevoegen</span>
						<input type="file" type="text" name="media" id="media">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text" name="media" id="media">
					</div>
				</div>
			</div>

			<div class="row">
				<input type="submit" class="waves-effect waves-light btn blue darken-3" id="uploadButton" name="uploadButton" style="width: 100%;">
			</div>

			</form>
		</div>
	</div>
</div>
<?php// } ?>
</body>
<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/jssocials.js"></script>
<script src="js/jssocials.shares.js"></script>
<script src="js/script.js"></script>
</html>