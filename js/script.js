$( document ).ready(function() {

    Materialize.fadeInImage('.welcome-msg');

    $(".tabs").on('click','.overview-tab',function (e) {

        var id = $(this).attr("id");

        $.post( "ajax/overview.php", { id: id})
        .done(function( response ){

            $('.overview-block').empty();
            $('.overview-block').append(response);
        });
    });

    $("#share").on('click','.jssocials-share',function (e) {
            var id = $(".hiddenId").val();
        $.post( "ajax/share.php", { id: id})


    });
    //changing fragment content
    $(".cd-container").on('click','.cd-timeline-content',function (e) {
        $(".welcome-msg").css("display","none");
        $(".content-block").css("display","block");
        //direct foto veranderen
        var id = $(this).attr("id");
        id = id.replace("media_", "");
        $.post( "ajax/content.php", { id: id})
            .done(function( response ){

                if(response.status != 'error'){

                    var array = response.status;
                    document.title = array[0][4];
                    if(array[0][2] == "quote"){
                        $('.video').hide();
                        $('.quote').show();
                        $('.quote').html("<div class='quote-block'><span class='quote-sign-start'>“</span>"+array[0][6]+"<span class='quote-sign-end'>”</span></div><span class='quotee'>"+ array[0][7]+"</span>");
                    }
                    else{
                        $('.quote').hide();
                        $('.video').show();
                        $('.share-hidden').html("<input class='hiddenId' type='hidden' value='"+ array[0][0] +"'>");
                        $('#content-title').html("<h4>" + array[0][4] + "</h4>");
                        $('#content-description').html("<p>" + array[0][6] + "</p>");
                        if(array[0][2] == "video"){
                            $('#content-source').html("<video width='100%' controls>"+
                                "<source src='videos/"+array[0][9]+"' type='video/mp4'></video>");
                        }else{
                            $('#content-source').html("<img src='images/"+array[0][9]+"'>")
                        }

                    }

                    var partsOfStr = array[0][7].split(',');

                    var Tstring = "";

                    for (var i=0; i < partsOfStr.length; i++){
                        if(partsOfStr[i] != "") {
                            Tstring = Tstring + "<div class='chip'>" + partsOfStr[i] + "</div>";
                        }
                    }

                    var partsOfStr2 = array[0][8].split(',');


                    for (var i=0; i < partsOfStr2.length; i++){
                        if(partsOfStr2[i] != ""){
                            Tstring = Tstring + "<div class='chip'>"+partsOfStr2[i]+"</div>";
                        }

                    }

                    console.log(Tstring);
                    $('.personages').html(Tstring);
                }



            });

        $('#info').css("display","block");
        $('.mobile-filter').css("display","none");


    });
    $("#info").on('click','#btn-back',function (e) {
        $('.mobile-filter').css("display","block");
        $('#info').css("display","none");
    });
    $("#timeline").on('click','#btn-filter-close',function (e) {
        $('.mobile-filter').css("display","none");
        $('.btn-filter-open').css("display","block");
    });
    $("#timeline").on('click','.btn-filter-open',function (e) {
        $('.mobile-filter').css("display","block").slideDown("slow");
        $('.btn-filter-open').css("display","none");
    });

    $("#info").on('click','.btn-filter',function (e) {


        //direct foto veranderen

        var jaar = $('.selector-season2').val();
        var personage= $('#select-person2').val();
        var gebeurtenis = $('#select-event2').val();
        var type = $('#select-type2').val();
        console.log(jaar+personage+gebeurtenis+type);
        $.post( "ajax/filter.php", {
            jaar: jaar,
            personage: personage,
            gebeurtenis: gebeurtenis,
            type: type
        }).done(function( response ){

            $('#cd-timeline').empty();

            $('#cd-timeline').append(response);



            });

    });
    $(".mobile-filter").on('click','#btn-filter1',function (e) {


        //direct foto veranderen

        var jaar = $('.selector-season').val();
        var personage= $('#select-person').val();
        var gebeurtenis = $('#select-event').val();
        var type = $('#select-type').val();
        $.post( "ajax/filter.php", {
            jaar: jaar,
            personage: personage,
            gebeurtenis: gebeurtenis,
            type: type
        }).done(function( response ){

            $('#cd-timeline').empty();

            $('#cd-timeline').append(response);



        });

    });




    //Navbar
     $(".button-collapse").sideNav();
    
     $("#share").jsSocials({
            shares: ["email", "twitter", "facebook"]
     });
    
    var year = new Date().getFullYear();
    for (i = year; i > 1994; i--) {
    var row = "<option value="+i+">"+i+"</option>";
    $('#jaartallen1').append(row);
        $('#jaartallen2').append(row);
    }
    $('select').material_select();
	
	
});