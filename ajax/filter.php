<?php
session_start();

include_once('../classes/Media.class.php');



    $jaar = $_POST['jaar'];
    $personage = $_POST['personage'];
    $gebeurtenis = $_POST['gebeurtenis'];
    $type = $_POST['type'];
    $media = new Media();
    $output = "";
    $icon = "";
    $classIcon ="cd-movie";
    try{
        $query = $media->choseContent($jaar,$personage,$gebeurtenis, $type);
        foreach($query as $x){

                switch ($x['type']) {
                                        case "video":
                                            $icon = "<img src='images/icons/icon-movie.svg' alt='Movie'>";
                                            $classIcon = "cd-movie";
                                        break;
                                        case "photo":
                                            $icon = "<img src='images/icons/icon-picture.svg' alt='Photo'>";
                                            $classIcon = "cd-picture";
                                        break;
                                        case "quote":
                                            $icon = "<img src='images/icons/icon-quote.svg' alt='Quote'>";
                                            $classIcon = "cd-quote";
                                        break;
                                        default:
                                            $icon = "<img src='images/icons/icon-movie.svg' alt='Movie'>";
                                            $classIcon = "cd-movie";
                                        break;
                                }

            $output .= "<div class='cd-timeline-block'>
                            <div class='cd-timeline-img ".$classIcon."'>".$icon."</div>
                            <div id='media_".$x['fragmentNr']."' class='cd-timeline-content'>
                                <div class='overlay-black'></div>
                                <img src='images/covers/".$x['cover']."' alt=''>
                                <div class='white-text pushup'>
                                    <div>
                                        <div class='content-title'>
                                            <h2 class='white-text'>Aflevering".$x['afleveringNr']."</h2>
                                            <span class='cd-date'>".$x['datum']."</span>
                                        </div>
                                        <p>".$x['title']."</p>
                                    </div>
                                    <a href='#0' class='cd-timeline-play'><img src='images/play.svg' alt=''></a>

                                </div>
                            </div> <!-- cd-timeline-content -->
                        </div> <!-- cd-timeline-block -->
";
        }

   echo $output;

    }catch(Exception $e){
        $feedback  = $e->getMessage();
        $response['status'] = 'error';
    }


?>