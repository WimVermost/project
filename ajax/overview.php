<?php
session_start();

include_once('../classes/Media.class.php');

if(!empty($_POST['id'])) {
    $media = new Media();

    $output = "";
    $tables = "";
    $tab = "";
    $title = "";

        if($_POST['id'] == "shares"){
            $query = $media->getShares();
            $tab = "shares";
            $title = "Meest gedeelde items";
        }else{
            $query = $media->getViews();
            $tab = "views";
            $title = "Meest bekeken items";
        }

        foreach($query as $mediaItem) {
            $tables .= "<tr>
				<td>".$mediaItem['fragmentNr']."</td>
				<td>".$mediaItem['event']."</td>
				<td>".$mediaItem['title']."</td>
				<td>".$mediaItem[$tab]."</td>
			  </tr>";
        }


        $output = "	<h5>$title</h5>


			<table class='highlighted'>
			<thead>
			  <tr>
				  <th data-field='fragmentNr'>Fragment nummer</th>
				  <th data-field='id'>Gebeurtenis</th>
				  <th data-field='name'>Titel</th>
				  <th data-field='price'>Aantal $tab</th>
			  </tr>
			</thead>
			<tbody>
			    $tables
			</tbody>
		  </table>";

        echo $output;




}
?>