<?php
session_start();

include_once('../classes/Media.class.php');

if(!empty($_POST['id'])) {
    $media = new Media();

    $media->FiD = $_POST['id'];

    try{
        $media->addShare();
        $response['status'] = 'success';
    }catch(Exception $e){
        $feedback  = $e->getMessage();
        $response['status'] = 'error';
    }
    header('Content-type: application/json');
    echo json_encode($response);

}
?>